require('dotenv').config();
const net = require('net')

const nodemailer = require('nodemailer');
const SCHEDULE_HEALTH_CHECK = "*/5 * * * *"
const CronJob = require('cron').CronJob;
const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASS_EMAIL
    }
});

let list_mail = ['nnhoang@cmc.com.vn', 'thieu@cmc.com.vn'];

let hosts = [
    {
        host: '202.134.19.34',
        port: '6379'
    },
    {
        host: '202.134.19.34',
        port: '5432'
    },
    {
        host: '183.91.11.56',
        port: '6379'
    },
    {
        host: '202.134.19.34',
        port: '5432'
    }
];
let sockets = [];
const cronjobUpdateFailed = new CronJob(SCHEDULE_HEALTH_CHECK, async function() {
        hosts.forEach(function(host, index){
            console.log(host)
            sockets[index] = new net.Socket()

            sockets[index].connect(host.port, host.host, () => {
                console.log('Service is available')
            });
            sockets[index].on('error', function (err) {
                console.log('An error happened in socket' + err.stack);
                let msg = host.host + ':' + host.port + ' is dead \n'
                msg = msg + err.stack;
                list_mail.forEach(email => {
                    let mailOptions = {
                        from: process.env.EMAIL,
                        to: email,
                        subject: 'Error server ' + host.host + ':' + host.port,
                        text: msg
                    };
                    transporter.sendMail(mailOptions, function(error, info){
                        if (error) {
                            console.log(error);
                        } else {
                            console.log('Email sent: ' + info.response);
                        }
                    });
                })
            });
        });
    }, function () {
        console.log('JOB FAIL');
    },
    true,
    process.env.TIME_ZONE
);

cronjobUpdateFailed.start();




